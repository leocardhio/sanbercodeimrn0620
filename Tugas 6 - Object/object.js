//no.1
console.log("-------------------");
function arrayToObject(arr) {
    // Code di sini
    var now = new Date()
    var thisYear=now.getFullYear()

    for (var i=0;i<arr.length;i++){
        var peopleObj = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2]
        }
        if(thisYear-arr[i][3]>=0){
            peopleObj.age=thisYear-arr[i][3]
        } else {
            peopleObj.age="Invalid Birth Year"
        }
        console.log(i+1+". "+arr[i][0]+" "+arr[i][1]+" : ",peopleObj)
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
// Error case 
arrayToObject([]) // ""

//no.2
console.log("--------------------------------")
function shoppingTime(memberId, money) {
    // you can only write your code here!
    if (memberId==undefined || memberId==""){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money<50000) {
        return "Mohon maaf, uang tidak cukup"
    } else {
        var listPurchased=[]
        var changeMoney=money
        while (changeMoney>=50000 && listPurchased[listPurchased.length-1]!="Casing Handphone" ){
            switch(true){
                case changeMoney>=1500000: {listPurchased.push("Sepatu Stacattu");changeMoney-=1500000;break}
                case changeMoney>=500000: {listPurchased.push("Baju Zoro");changeMoney-=500000;break}
                case changeMoney>=250000: {listPurchased.push("Baju H&N");changeMoney-=250000;break}
                case changeMoney>=175000: {listPurchased.push("Sweater Uniklooh");changeMoney-=175000;break}
                case changeMoney>=50000: {listPurchased.push("Casing Handphone");changeMoney-=50000;break}
            }
        } 
        var memberObj = {
            memberId : memberId,
            money : money,
            listPurchased : listPurchased,
            changeMoney : changeMoney
        } 
        return memberObj
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//no.3
console.log("-----------------------")
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var data=[]
    for (var i = 0; i<arrPenumpang.length;i++){
        var ke= arrPenumpang[i][2];
        var dari = arrPenumpang[i][1];
        var bayar=(ke.charCodeAt(0)-dari.charCodeAt(0))*2000

        var dataObj={
            penumpang : arrPenumpang[i][0],
            naikDari : dari,
            tujuan : ke,
            bayar : bayar
        }
        data.push(dataObj)
    }
    return data
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]