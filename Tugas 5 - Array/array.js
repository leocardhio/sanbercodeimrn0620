//no.1
console.log("--------No.1---------");
// Code di sini
function range(startNum,finishNum){
    if (startNum<=finishNum){
        var arr=[]
        for (var i=startNum; i<=finishNum; i++){
            arr.push(i)
        }
        return arr
    } else if (startNum>finishNum) {
        var arr= []
        for (var i=finishNum; i<=startNum; i++){
            arr.unshift(i)
        }
        return arr
    } else {
        return -1
    }
} 

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//no.2
console.log("-------No.2-------");
// Code di sini
function rangeWithStep(startNum, finishNum, step){
    if (startNum<=finishNum){
        var arr=[]
        for (var i=startNum; i<=finishNum; i+=step){
            arr.push(i)
        }
        return arr
    } else if (startNum>finishNum) {
        var arr= []
        for (var i=startNum; i>=finishNum; i-=step){
            arr.push(i)
        }
        return arr
    } else {
        return -1
    }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//no.3
console.log("----------No.3----------");
// Code di sini
function sum(startNum, finishNum, step){
    if (startNum==undefined){
        return 0
    } else if (finishNum==undefined){
        return startNum
    } else if (step==undefined){
        var arr= rangeWithStep(startNum, finishNum, 1)
        sum1=0
        for (var i=0; i<arr.length; i++){
            sum1+=arr[i]
        }
        return sum1
    }  else {
        var arr= rangeWithStep(startNum, finishNum, step)
        sum1=0
        for (var i=0; i<arr.length; i++){
            sum1+=arr[i]
        }
        return sum1
    }
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//no.4
console.log("--------No.4---------");
//contoh input
function dataHandling(input){
    for (var i=0; i<input.length;i++){
        console.log("Nomor ID: "+input[i][0])
        console.log("Nama Lengkap: "+input[i][1])
        console.log("TTL : "+ input[i][2]+" "+input[i][3])
        console.log("Hobi: "+ input[i][4])
        console.log()
        }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

dataHandling(input)

//no.5
console.log("-----------No.5-----------");
// Code di sini
function balikKata(kata){
    maxidx=kata.length-1
    var arr=[]
    for (var i=maxidx;i>=0;i--){
        arr+=kata[i]
    }
    return arr
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

//no.6
console.log("----------No.6----------");
function dataHandling2(input){
    data=input.slice()
    data[1]="Roman Alamsyah Elsharawy"
    data[2]="Provinsi Bandar Lampung"
    data.splice(4,1,"Pria","SMA Internasional Metro")
    console.log(data)

    var tgl= input[3].split("/")
    switch(tgl[1]){
        case "01":  { console.log("Januari"); break;}
        case "02":  { console.log("Februari"); break;}
        case "03":  { console.log("Maret"); break;}
        case "04":  { console.log("April"); break;}
        case "05":  { console.log("Mei"); break;}
        case "06":  { console.log("Juni"); break;}
        case "07":  { console.log("Juli"); break;}
        case "08":  { console.log("Agustus"); break;}
        case "09":  { console.log("September"); break;}
        case "10":  { console.log("Oktober"); break;}
        case "11":  { console.log("November"); break;}
        case "12":  { console.log("Desember"); break;}
    }
    var sorted=tgl.slice()
    console.log(sorted.sort(function(value1,value2){return value2-value1}))

    console.log(tgl.join("-"))

    var nama=input.slice(1,2)
    console.log(nama.slice(0,15))
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 