//no.1
console.log("----------------NO.1--------------------")
var i = 1;
var j = 1;
while(i<=2) {
    if (i==1) {
        console.log("LOOPING PERTAMA")
        while (j<=10){
            console.log(String(j*2)+" - I love coding")
            j++;
        }
    } else {
        console.log("LOOPING KEDUA")
        j=10
        while (j>=1){
            console.log(String(j*2)+" - I will become a mobile developer")
            j--;
        }
    }
    i++;
}

//no.2
console.log("----------------NO.2--------------------")
for(var i=1; i<=20; i++){
    if((i%2==1) && (i%3==0)){
        console.log(String(i)+" - I Love Coding")
    } else if (i%2==1){
        console.log(String(i)+" - Santai")
    } else {
        console.log(String(i)+" - Berkualitas")
    }
}

//no.3
console.log("----------------NO.3--------------------")
for(var i=0;i<4;i++){
    console.log("########")
}

//no.4
console.log("----------------NO.4--------------------")
for(var i=0;i<7;i++){
    var pagar = "#"
    for(var j=0;j<i;j++){
        pagar=pagar.concat("#")
    }
    console.log(pagar)
}

//no.5
console.log("----------------NO.5--------------------")
for(var i=0;i<8;i++){
    var baris = ""
    for(var j=0;j<8;j++){
        if(i%2==0){//baris genap (A)
            if(j%2==0){ //kolom genap (A)
                baris = baris.concat(" ")
            } else { //kolom ganjil (A)
                baris = baris.concat("#")
            }
        } else { //baris ganjil (B)
            if(j%2==0){//kolom genap (B)
                baris = baris.concat("#")
            } else { //kolom ganjil (B)
                baris = baris.concat(" ")
            }
        }
    }
    console.log(baris)
}