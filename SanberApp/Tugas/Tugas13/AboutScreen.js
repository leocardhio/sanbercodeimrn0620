import React from 'react';
import {View,Text,Image,StatusBar,TextInput, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon2 from 'react-native-vector-icons/FontAwesome5'

export default class App extends React.Component{
    render(){
        return(
            <View style={{flex:1}}>
                <StatusBar/>
                <ScrollView>
                    <View style={{paddingTop:64,alignItems:'center'}}>
                        <Text style={{fontSize:36,color:'#003366',fontWeight:'bold',paddingBottom:12}}>Tentang Saya</Text>
                    </View>
                    <View style={{height:200,width:200,backgroundColor:'#efefef',alignSelf:'center',borderRadius:100}}>
                        <Icon name='account' size={190} color={'#CACACA'} style={{alignSelf:'center',justifyContent:'center'}}/>
                    </View>
                    <View style={{paddingTop:24,alignItems:'center'}}>
                        <Text style={{fontSize:24,fontWeight:'bold',color:'#003366'}}>Mukhlis Hanafi</Text>
                        <Text style={{paddingBottom:16,paddingTop:8,fontSize:16,fontWeight:'bold',color:'#3ec6ff'}}>React Native Developer</Text>
                    </View>
                    <View style={{height:140,width:359,alignSelf:'center',backgroundColor:'#efefef',borderRadius:16}}>
                        <Text style={{fontSize:18, paddingLeft:8,paddingTop:5,color:'#003366',paddingBottom:5}}>Portofolio</Text>
                        <View style={{height:0,width:343,borderWidth:1,borderColor:'#003366',alignSelf:'center'}}/>
                        <View style={{justifyContent:'space-around',alignItems:'center',flexDirection:'row'}}>
                            <View>
                                <Icon name='gitlab' size={45} color={'#3ec6ff'} style={{paddingTop:15,marginRight:12}}/>
                                <Text style={{paddingTop:8,fontSize:15,fontWeight:'bold',color:'#003366'}}>@mukhlish</Text>
                            </View>
                            <View>
                                <Icon2 name='github' size={45} color={'#3ec6ff'} style={{paddingTop:15,marginLeft:20}}/>
                                <Text style={{paddingTop:8,fontSize:15,fontWeight:'bold',color:'#003366'}}>@mukhlis-h</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{height:259,width:359,marginTop:8,alignSelf:'center',backgroundColor:'#efefef',borderRadius:16}}>
                        <Text style={{fontSize:18, paddingLeft:8,paddingTop:5,color:'#003366',paddingBottom:5}}>Hubungi Saya</Text>
                        <View style={{height:0,width:343,borderWidth:1,borderColor:'#003366',alignSelf:'center'}}/>
                        <View style={{marginTop:18,alignItems:'center'}}>
                            <View style={{flexDirection:'row'}}>
                                <Icon2 name='facebook' color={'#3ec6ff'} size={45}/>
                                <Text style={{fontSize:16,fontWeight:'bold', marginTop:13, marginLeft:20, color:'#003366'}}>mukhlis.hanafi</Text>
                            </View>
                            <View style={{flexDirection:'row',marginTop:30,marginLeft:15}}>
                                <Icon2 name='instagram' color={'#3ec6ff'} size={45}/>
                                <Text style={{fontSize:16,fontWeight:'bold', marginTop:13, marginLeft:20, color:'#003366'}}>@mukhlis_hanafi</Text>
                            </View>
                            <View style={{flexDirection:'row',marginTop:30,marginRight:30}}>
                                <Icon2 name='twitter' color={'#3ec6ff'} size={45}/>
                                <Text style={{fontSize:16,fontWeight:'bold', marginTop:13, marginLeft:20, color:'#003366'}}>@mukhlish</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>    
            </View>
        )
    }
}