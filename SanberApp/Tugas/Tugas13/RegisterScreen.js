import React from 'react';
import {View,Text,Image,StatusBar, ScrollView,TextInput, TouchableOpacity} from 'react-native';

export default class App extends React.Component{
    render(){
        return(
            <View style={{flex: 1,backgroundColor:'e5e5e5'}}>
                <ScrollView>    
                    <StatusBar/>
                    <View style={{paddingTop:63}}>
                    <Image source={require('./assets/logo.png')} style={{resizeMode:'cover',height:92,width:355}}/>
                    </View>
                    <View style={{paddingTop: 70,alignItems:'center'}}>
                        <Text style={{fontSize:24,color:'#003366'}}>Register</Text>
                    </View>
                    <View style={{paddingLeft:30, paddingTop:40,flexDirection:"column"}}>
                        <Text style={{fontSize:16,color:'#003366'}}>Username/Email</Text>
                        <View style={{height:48,width:294,alignItems:'flex-start',borderWidth:1,borderColor:'#003366'}}>
                            <TextInput style={{paddingVertical:7,paddingLeft:10,textAlign:'left',fontSize:24}}></TextInput>
                        </View>
                        <Text style={{fontSize:16,paddingTop:16,color:'#003366'}}>Email</Text>
                        <View style={{height:48,width:294,alignItems:'flex-start',borderWidth:1,borderColor:'#003366'}}>
                            <TextInput style={{paddingVertical:7,paddingLeft:10,textAlign:'left',fontSize:24}}></TextInput>
                        </View>
                        <Text style={{fontSize:16,paddingTop:16,color:'#003366'}}>Password</Text>
                        <View style={{height:48,width:294,alignItems:'flex-start',borderWidth:1,borderColor:'#003366'}}>
                            <TextInput style={{paddingVertical:7,paddingLeft:10,textAlign:'left',fontSize:24}}></TextInput>
                        </View>
                        <Text style={{fontSize:16,paddingTop:16,color:'#003366'}}>Ulangi Password</Text>
                        <View style={{height:48,width:294,alignItems:'flex-start',borderWidth:1,borderColor:'#003366'}}>
                            <TextInput style={{paddingVertical:7,paddingLeft:10,textAlign:'left',fontSize:24}}></TextInput>
                        </View>
                    </View>
                    <View style={{paddingTop:32,alignItems:'center'}}>
                        <TouchableOpacity> 
                            <View style={{height:40,width:140,backgroundColor:'#003366',alignItems:'center',justifyContent:"center",borderRadius:16}}>
                                <Text style={{fontSize:24,color:'#ffffff'}}>Daftar</Text>
                            </View>
                        </TouchableOpacity>        
                        <Text style={{paddingTop:16,paddingBottom:16,fontSize:24,color:'#3ec6ff',alignItems:"center"}}>atau</Text>
                        <TouchableOpacity>
                            <View style={{height:40,width:140,backgroundColor:'#3ec6ff',alignItems:'center',justifyContent:"center",borderRadius:16}}>
                                <Text style={{fontSize:24,color:'#ffffff'}}>Masuk?</Text>
                            </View>
                        </TouchableOpacity>
                        <View style={{paddingBottom:77}}/>
                    </View>
                </ScrollView>    
            </View>
        )
    }
}