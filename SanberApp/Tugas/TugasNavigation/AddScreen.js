import React from 'react'
import {Text,StyleSheet,View} from 'react-native'

const ScreenContainer = ({ children }) => (
    <View style={styles.container}>{children}</View>
  );

export default function Add(){
    return(
        <ScreenContainer>
            <Text>Halaman Tambah</Text>
        </ScreenContainer>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#fff",
      alignItems: "center",
      justifyContent: "center"
    }
  });  