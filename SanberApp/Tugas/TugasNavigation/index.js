import React from 'react';
import {StyleSheet,View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import {createDrawerNavigator} from '@react-navigation/drawer'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import Login from './LoginScreen'
import About from './AboutScreen'
import Add from './AddScreen'
import Project from './ProjectScreen'
import Skill from './SkillScreen'

const Drawer = createDrawerNavigator();
const LoginStack = createStackNavigator();
const Tabs=createBottomTabNavigator();

const TabsScreen = ()=>(
    <Tabs.Navigator>
        <Tabs.Screen name='Skill' component={Skill}/>
        <Tabs.Screen name='Project' component={Project}/>
        <Tabs.Screen name='Add' component={Add}/>
    </Tabs.Navigator>
)

export default function App() {
    return(
        <NavigationContainer>
            <Drawer.Navigator>
                <Drawer.Screen name={'Log In'} component={Login}/>
                <Drawer.Screen name={'About Me'} component={About}/>
                <Drawer.Screen name={'Additional Info'} component={TabsScreen}/>
            </Drawer.Navigator>
        </NavigationContainer>
    )
}
