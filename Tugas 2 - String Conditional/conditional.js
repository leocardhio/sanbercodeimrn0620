var nama="";
var peran="";


var namafix=nama.trim()
var peranfix=peran.trim()
if (namafix=="") {
    console.log("Nama harus diisi!")}
else {
    if (peranfix=="") {
        console.log("Halo "+nama+", Pilih peranmu untuk memulai game!")}
    else if (peranfix.toLowerCase()=="penyihir"){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!")}
    else if (peranfix.toLowerCase()=="guard"){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf")}
    else if (peranfix.toLowerCase()=="werewolf"){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!")}
    else {
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Maaf, peran tidak tersedia")}
    }

//Switch Case
var tanggal = 21; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 10; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2000; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

if ((tanggal<=31) && (tanggal>=1)) {
    if ((tahun>=1900) && (tahun<=2200)){
        switch(bulan){
            case 1: {console.log(String(tanggal)+" Januari "+String(tahun)); break;}
            case 2: {console.log(String(tanggal)+" Februari "+String(tahun)); break;}
            case 3: {console.log(String(tanggal)+" Maret "+String(tahun)); break;}
            case 4: {console.log(String(tanggal)+" April "+String(tahun)); break;}
            case 5: {console.log(String(tanggal)+" Mei "+String(tahun)); break;}
            case 6: {console.log(String(tanggal)+" Juni "+String(tahun)); break;}
            case 7: {console.log(String(tanggal)+" Juli "+String(tahun)); break;}
            case 8: {console.log(String(tanggal)+" Agustus "+String(tahun)); break;}
            case 9: {console.log(String(tanggal)+" September "+String(tahun)); break;}
            case 10: {console.log(String(tanggal)+" Oktober "+String(tahun)); break;}
            case 11: {console.log(String(tanggal)+" November "+String(tahun)); break;}
            case 12: {console.log(String(tanggal)+" Desember "+String(tahun)); break;}
        }
    } else {
        console.log("Tahun tidak valid")
    }
} else {
    console.log("Tanggal tidak valid")
}